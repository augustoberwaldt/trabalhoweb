<?php

class Load {

	public function loadModel($model, $object =null){
      
        if (is_array($model)) {
            foreach($model as $value) {
                Load::loadm($value , $object);
            }
        } else {
            Load::loadm($model , $object);
        }
	   

	}
    
    public function  loadm($model , $object =null) 
    {
        
        $pathModel = APP.DS.'model'.DS.$model.'.php';
    
		if(!file_exists($pathModel)){
				return false;
	    }
	
	    include_once($pathModel);

		$controller = ClassRegistry::getInstance();
        
		if ($object != null) {
	
			$object->$model = new $model();
			return false;
		}
		
		$controller = $controller->objects['controller'];
		$controller->$model = new $model();
        
    }
    
	public static function loadClass($class){

	   $pathClass = APP.DS.'class'.DS.$class.'.php';

       if(!file_exists($pathClass)){
			return false;
	   }
    
       include $pathClass;
       $controller = ClassRegistry::getInstance();
       $controller = $controller->objects['controller'];        
      
       $controller->{'class'.$class} = new $class(); 
	   return $controller->{'class'.$class};
	}

	public static function loadView($view)
	{
	   $pathView = VIEW.DS.$view.'.php';
       if(!file_exists($pathView)){
			return false;
	   }
       require_once $pathView;
       return true;
	}
	public static function loadViewHelper($class)
	{
	   $pathView = APP.DS.'viewHelper'.DS.$class.'.php';
       if(!file_exists($pathView)){
			return false;
	   }
       require_once $pathView;
       return true;
	}


}
