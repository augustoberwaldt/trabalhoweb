<?php

class Router {

    
	public  function rota($url) 
	{
		$url = preg_replace('/^[\/]|[\/]$/',"",$url);
		
		if ($url == 'index' || $url == '/' ) {
			return [];
		}
		
		$rotas = [
			'produtos' => ['action' => 'index' , 'controller' => 'produtos']
		     
		];
		
		$url = preg_replace('/((&|\?)(.*)=[^&]*)/', "", $url );
		
		
		Load::loadModel(['ProdutosModel', 'CategoriaModel'], $this);
        $idcat = '';
		if (!empty($url))
			$idcat = $this->CategoriaModel->find([
				'campos'=> ['id'], 
				'limite'  => 1 ,
				'filtro' => 'link like'."'%".$url ."%'" 
			]);
		
		
        $filtroCat = '';
		if (!empty($idcat) && isset($idcat[0]['id'])) {
            $filtroCat = ' OR  id_categoria = '. $idcat[0]['id'];
        }
        
		$link = $this->ProdutosModel->find([
		    'campos'=> ['id'], 
			'filtro' => 'link = '."'". $url ."'". $filtroCat 
		]);
         
	
		if (!empty($link) && empty($idcat)) {
			$this->controller = 'produtos';
			$this->action     = 'detalhe';
			return $link;
		} elseif(!empty($link))
		{  
			$this->controller = 'produtos';
			$this->action     = 'index';
			return $link;
			
		}else{
			$this->controller = 'index';
			$this->action     = 'index';
			return;
		}
		
		

	}

}
