﻿<?php
require_once LIB.DS.'Router.php';

class Dispatcher {

    const  ACTION_PADRAO  = 'index';
	const  CONTROLLER_PADRAO  = 'index';
   
    public $controller;
    public  $action;	
	public function Dispatcher() 
	{
        $this->getActionController();
	
		$rota = Router::rota($this->getbaseUrl());
        if (!$rota)
			$this->getActionController();
		
 	    $this->controller = preg_replace('/((&|\?)(.*)=[^&]*)/', "", $this->controller );	
		$controller_class = ucwords($this->controller).'Controller';
		$model_class = ucwords($this->controller).'Model';
    	
		$file = CONTROLLER.DS.$controller_class . '.php';
		$model= MODEL.DS.$model_class.'.php';
	  
		if (!file_exists($file)) {
			$this->pageNotFound();
		}
     		 
		require_once  $file;
			
	    $object = new $controller_class();   
		if (!method_exists($object,$this->action)){
			$this->pageNotFound();
		}
		if(file_exists($model)){
     	   require_once $model;
		   if (class_exists($model_class)) {
				    
				$model_obj = new $model_class();	         
				$model_obj->setController($object);
				$object->$model_class = $model_obj;
					
		   }
		}
		
		session_start();
       
        $_GET['produtos'] = $rota;
		$data = array_merge($_GET, $_POST);
        if (empty($data['produtos'])) {
			unset($data['produtos']);
		}  
		$object->data = $data;

	  call_user_func_array([$object, $this->action], ['get' => $_GET ,'post' =>$_POST]);

		if ($object->useView()) {
			$object->render();
		}

		unset($object);
	}

	private function pageNotFound()
	{
		header("HTTP/1.0 404 Not Found");
		Load::loadView('404');
		exit();
	}

  private function getActionController()
	{
		$request = $this->getbaseUrl();
		$params     = explode('/',$request);
	     
		$action     = empty($params[2]) ? self::ACTION_PADRAO : $params[2];
		$controller = empty($params[1]) ? self::CONTROLLER_PADRAO :   $params[1];
	    $action = preg_replace('/((&|\?)(.*)=[^&]*)/',"",$action);
	   
	    $this->controller = $controller;
		$this->action     =  $action;

		return [
			'action'     => $action,
		  'controller' => $controller
	  ];

	}
    private  function getbaseUrl() 
	{
		$request    = str_ireplace($_SERVER['SERVER_NAME'],"",$_SERVER['REQUEST_URI']);
		$request    = str_ireplace(['/projeto/trabalhoweb/', '/trabalhoweb/'],	"/",	$request);
		
		return  	$request;
	}


}
