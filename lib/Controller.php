<?php
abstract class Controller  {
	protected $view = NULL;
	private $viewVars = array();
	private $action;
    protected $helpers;

	public function __construct()
	{
		ClassRegistry::set('controller', $this);
		$this->setViewHelper(); 
	}

    public function setViewHelper() 
    {
 		Load::loadViewHelper('MainViewHelper');    
 		$mainHelper = new  MainViewHelper();       
        $mainHelper->_controller = &$this;
        $this->helpers['Main'] = $mainHelper ;                
    }
	public final function render() {

		$file_view = VIEW.DS.$this->view.'.php';
		if(!file_exists($file_view)) {
			die("Crie a view no diretorio: " . $file_view);
		}
        $returnView  = array_merge($this->viewVars, $this->helpers);  
	
		extract($returnView);
          
		require_once($file_view);
	}

	protected final function set($name,$value) {
		$this->viewVars[$name] = $value;
	}
	protected final function setVars($array) {
		$this->viewVars = $array;
	}
	public function useView() {
		return $this->view != NULL;
	}

	protected final function remove($name) {
		if(isset($this->viewVars[$name])) {
			unset($this->viewVars[$name]);
			return true;
		}
		return false;
	}
    
    public function __call($name, $arguments)
    {
        die('method not exists');
    }
    
}
