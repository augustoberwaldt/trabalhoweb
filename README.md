![Build Status](https://gitlab.com/augustoberwaldt/trabalhoweb/badges/master/build.svg)


# IFRS Canoas

## TrabalhoWeb E-commerce


### Intalação

```sh
$ git clone [git-repo-url]  trabalhoweb
$ cd trabalhoweb
$ composer install
```

Extensoes PHP que dever ser  Habilitadas no Arquivo PHP.ini:

````
php_pdo_pgsql.dll
````

###  Testando a aplicação
* Não esqucer a porta que o servidor web esta rodando.
 exemplo com porta :
```
http://localhost:8080/trabalhoweb
```
Exemplo sem porta: 
```
http://localhost/trabalhoweb
```