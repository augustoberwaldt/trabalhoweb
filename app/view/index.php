<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Autopecas</title>

   </head>
   <body>
   <?php   $Main->element('topo',['Main' => $Main ]);  ?>

	  <div class="main-banner" >
		  <ul class="bxslider">
				<li><img width="100%"	height="300px" src="https://d24kgseos9bn1o.cloudfront.net/girardi/imagens/banners/img/e7a88226a1fdb6f4120aa2d7e17ff240.jpg"/></li>
				<li><img width="100%"	height="300px" src="https://d24kgseos9bn1o.cloudfront.net/girardi/imagens/banners/img/69de8ca651b8cec64606297782aadd16.jpg"/></li>
				<li><img width="100%"	height="300px" src="https://d24kgseos9bn1o.cloudfront.net/girardi/imagens/banners/img/c3b3d5a17d894360635394be20afeaf6.jpg"/></li>
				<li><img width="100%"	height="300px" src="https://d24kgseos9bn1o.cloudfront.net/girardi/imagens/banners/img/2e9c07cffbbe169b876d78d5f8a550c7.jpg"/></li>
		  </ul>
      </div>

     <!-- Destaques -->
      <div  class="main-destaques" >
	    <h2> Destaques </h2>
		  <?php 
 
		    

	      ?>
		  <?php foreach($produtos as $destaques) :   ?>

			<a href="<?=  $destaques['link'] ; ?>">
			   <div style=" border: 1px solid #D5D2D6; width:18%; height:300px; margin-left:22px; float:left !important; " >
		          <div>
                        <img  height="200px" src ="<?= $destaques['imagem'] ; ?>" />
				  </div>
				  <div>
				      <p style="    text-align: center;"> <?=  $destaques['titulo'] ; ?> </p>
				  </div>
				  <div class="btncomprar">
				      <a href="<?=  $destaques['link'] ; ?>" > <img src="http://3.bp.blogspot.com/-OYzhBznmTMo/VOzMaM7_xAI/AAAAAAAABXI/2nK8vXctjEs/s1600/boton-com5555pra.png" alt="COMPRAR" style="border: none; width:100px; height:40px; padding:0;"/></a>
				  </div>
		      </div>
			</a>
		  <?php endforeach;  ?>
	  </div>
	  <hr>
      <!-- Mais vendidos -->
      <div  class="main-mais-vendidos" >
	    <h2> Mais Vendidos </h2>
	        <?php

			foreach($maisvendidos as $maisVendido) :   ?>

			<a href="<?= $maisVendido['link'] ; ?>">
				<div style="border: 1px solid #D5D2D6; width:18%; height:300px; margin-left:22px; float:left !important; ">
				    <div>
                        <img height="200px" src ="<?= $maisVendido['imagem'] ; ?>" />
				    </div>
				    <div>
				      <p style="    text-align: center;"> <?=  $maisVendido['titulo'] ; ?> </p>
				    </div>
				    <div class="btncomprar">
				         <a href="<?= $maisVendido['link'] ; ?>" ><img src="http://3.bp.blogspot.com/-OYzhBznmTMo/VOzMaM7_xAI/AAAAAAAABXI/2nK8vXctjEs/s1600/boton-com5555pra.png" alt="COMPRAR" style="border: none; width:100px; height:40px; padding:0;"/>
				    </div>
				</div>
			</a>

		  <?php endforeach;  ?>
	  </div>
   <?php $Main->element('rodape');  ?>

   </body>
</html>
