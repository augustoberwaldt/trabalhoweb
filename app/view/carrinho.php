<?php   $Main->element('topo' ,['Main' => $Main ]); ?>
    <title>Carrinho de Compras </title>
    <style>
        table {
            border: 1px solid #ddd;
            padding: 5px;
        }
        table th {
            background-color: #ddd;
        }
        table td {
            padding: 5px;
            background-color: #fff;
            text-align: center;
        }
        table td input {
            text-align: center;
            padding: 3px;
        }
        .btnComprar{
           text-decoration:none;
           text-align:center;
           display:block; 
           background-color:#c00a26;width:130px; 
           height:35px; 
           color:white;
           line-height: 30px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<?= SERVER_VIEW ?>/public/css/home.css">		
	</head>
	<body>
		<div id="container" style=" clear:both; height:400px; width: 100%; margin: 0 auto; position:relative;">
			<table cellpadding="6" cellspacing="1" style="width:100%" border="1" align="center">
			<tr>
			  <th>Produto</th>
			  <th>Quantidade (unidade)</th>
			  <th>Preço R$  (unitário)</th>
			  <th>Sub-Total R$ (do item)</th>
			  
			</tr>
				<?php $val = 0; ?>
				
				<?php $produto = $Main->getCarrinho(); 
				if(!empty($produto['produtos'])) 
					foreach ($produto['produtos'] as  $value) : ?>
				<tr>
				  <td><?php echo $value["titulo"]; ?></td>
				  <td><?php echo $value["qtd"]; ?> </td>
				  <td><?php echo $Main->currency($value["preco"], false); ?></td>
				  <td><?php echo $Main->currency($value["preco"] * $value["qtd"],false); ?></td>
				  <?php $val = $val + ($value["preco"] * $value["qtd"]); ?>
				</tr>
				<?php  endforeach;?>
			<tr>
				<td colspan="2">&nbsp;</td>
				<td style="background-color: #ddd;"><strong>Valor Total da Compra R$ </strong></td>
	            <td style="background-color: #ddd;"><?php echo $Main->currency($val); ?></td>
			</tr>
            <tr>
                <td colspan="4">
                    <div style="float: left; padding-top: 6px;"><a href="<?php echo SERVER_VIEW; ?>/carrinho/destroy">Limpar Carrinho</a></div>
                </td>
            </tr>
			</table>
            <div style="float:right; position:absolute; bottom:10; right:10;" >
               <a href="<?= SERVER_VIEW ?>/pagamento/index" class="btnComprar"> 
                    Continuar 
               </a>
            </div>
		</div>
 <?php $Main->element('rodape');  ?>
