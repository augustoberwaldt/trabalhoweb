     <div class="main-topo">
		 <div class="topo">
		   <div class="tp-list-opt">
			  <ul class="tp-atd">
				 <li class="tp-list-item"> <a  class ="tp-link-list" href="#"> (51) 34777779</a> </li>
				 <li class="tp-list-item"> <a  class ="tp-link-list" href="<?php echo SERVER_VIEW; ?>/atendimento/index"> Central de Atendimento</a> </li>
				 <li class=" tp-list-item tp-list-item-login"> 
                    Minha conta
                    <?php if (!$Main->getClienteLogado()) :?>
                        <?php $Main->element('login-drop'); ?> 
                    <?php else:?>
                        <?php $Main->element('logar', ['Main' => $Main]); ?> 
                    <?php endif;?>
                 </li>
			  </ul>
		   </div> 
			<!-- menu principal  -->
			 <div class="main-principal">
					<div class="logo">
					   <a href="<?php echo SERVER_VIEW; ?>/index" ><img  class="img-logo" src="http://www.freelogoservices.com/api/main/images/1j+ojl1FOMkX9WypfBe43D6kj...KCpR5MnBbOwXs1M3EMoAJtlCYvgPZs9f45"> </a>
					</div>
					<div class="busca">
					   <form class="formbusca" action="<?php echo SERVER_VIEW; ?>/produtos">
						  <input type="text" name="busca" autocomplete="off" class="txtbusca"  autocomplete="off" placeholder="   O que você está procurando ?">
						  <button type="submit" title="Buscar" class="btn-search"></button>
					   </form>
					</div>
					<div class="carrinho">
					   <a href="<?php echo SERVER_VIEW; ?>/Carrinho/index"><i class="icn-cart"> </i> </a>
					   <div class="txt-cart">
                       <?php $total = $Main->getTotalCarrinho();
                            if ($total > 0) : ?>
                            <?php echo $total . ' Item'; ?>  
                       <?php endif; ?>                         
                        <br>
                       Meu carrinho 
                       </div>
					</div>
			 </div>
		</div>
		<!-- menu categorias-->
		<div class="main-menu-top">
				<div class="center">
				   <ul >
				      <?php
				    
                         $categorias = $Main->getBloco('categoria');      
				         foreach ($categorias as $key => $value) :  ?> 
					        <li ><a href="<?=  SERVER_VIEW. DS . $value['link']?>"> <?= $value['titulo'] ?></a></li>
				      <?php  endforeach; ?>
				   </ul>
				</div>
		</div>
	</div>
