<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title><?= $title; ?></title>
   <style>
    .form-usuarios{
        color: #969696;        
        border: 1px solid #D5D2D6;
        border-radius: 5px;
    }
   </style>   
   </head>
   <body>
   <?php   $Main->element('topo',['Main' => $Main ]);  ?>
<form class="form-usuarios" action= "<?php echo SERVER_VIEW;?>/clientes/salvar" method="post">

<!-- DADOS PESSOAIS-->
 <table cellspacing="10"; style="margin-left:500px">
  <tr>
   <td>
    <label for="nome">Nome: </label>
   </td>
   <td align="left">
    <input  class ="form-usuarios" type="text" name="nome">
   </td>
   </tr>
   <tr>
   <td>
    <label for="sexo">Sexo:</label>
   </td>
   <td align="left">
    <select class ="form-usuarios" name="sexo"> 
    <option value="M">Masculino</option> 
    <option value="F">Feminino</option> 
   </select>
   </td>
  </tr>
  <tr>
   <td>
    <label>Nascimento: </label>
   </td>
   <td align="left">
   <input class ="form-usuarios" type="text" name="dia" size="2" maxlength="2" value="dd"> 
   <input class ="form-usuarios"type="text" name="mes" size="2" maxlength="2" value="mm"> 
   <input class ="form-usuarios"type="text" name="ano" size="4" maxlength="4" value="aaaa">
   </td>
  </tr>
  <tr>
   <td>
    <label for="telefone">Telefone: </label>
   </td>
   <td align="left">
    <input class ="form-usuarios" type="text" name="codarea" size="2" maxlength="2"> 
    <input class ="form-usuarios" type="text" name="telefone" size="8" maxlength="8"> 
   </td>
  </tr>

<!-- ENDEREÇO -->
  <tr>
   <td>
    <label for="rua">Rua:</label>
   </td>
   <td align="left">
    <input  class ="form-usuarios" type="text" name="rua">
   </td>
  </tr>
  <tr>
   <td>
    <label for="numero">Numero:</label>
   </td>
   <td align="left">
    <input class ="form-usuarios" type="text" name="numero" size="4">
   </td>
   </tr>
   <tr>
    <td>
    <label for="completo">Complemento:</label>
   </td>
   <td align="left">
    <input  class ="form-usuarios" type="text" name="complemento" size="10">
   </td>
  </tr>
  <tr>
   <td>
    <label for="bairro">Bairro: </label>
   </td>
   <td align="left">
    <input class ="form-usuarios" type="text" name="bairro">
   </td>
  </tr>
  <tr>
   <td>
    <label for="cidade">Cidade: </label>
   </td>
   <td align="left">
    <input  class ="form-usuarios" type="text" name="cidade">
   </td>
  </tr>
  <tr>
   <td>
    <label for="estado">Estado:</label>
   </td>
   <td align="left">
    <select class ="form-usuarios" name="estado"> 
    <option value="ac">Acre</option> 
    <option value="al">Alagoas</option> 
    <option value="am">Amazonas</option> 
    <option value="ap">Amapá</option> 
    <option value="ba">Bahia</option> 
    <option value="ce">Ceará</option> 
    <option value="df">Distrito Federal</option> 
    <option value="es">Espírito Santo</option> 
    <option value="go">Goiás</option> 
    <option value="ma">Maranhão</option> 
    <option value="mt">Mato Grosso</option> 
    <option value="ms">Mato Grosso do Sul</option> 
    <option value="mg">Minas Gerais</option> 
    <option value="pa">Pará</option> 
    <option value="pb">Paraíba</option> 
    <option value="pr">Paraná</option> 
    <option value="pe">Pernambuco</option> 
    <option value="pi">Piauí</option> 
    <option value="rj">Rio de Janeiro</option> 
    <option value="rn">Rio Grande do Norte</option> 
    <option value="ro">Rondônia</option> 
    <option value="rs">Rio Grande do Sul</option> 
    <option value="rr">Roraima</option> 
    <option value="sc">Santa Catarina</option> 
    <option value="se">Sergipe</option> 
    <option value="sp">São Paulo</option> 
    <option value="to">Tocantins</option> 
   </select>
   </td>
  </tr>
  <tr>
   <td>
    <label for="cep">CEP: </label>
   </td>
   <td align="left">
    <input class ="form-usuarios" type="text" name="cep" size="5" maxlength="5"> - <input class ="form-usuarios" type="text" name="cep2" size="3" maxlength="3">
   </td>
  </tr>

<!-- DADOS DE LOGIN -->
  <tr>
   <td>
    <label for="email">E-mail: </label>
   </td>
   <td align="left">
    <input class ="form-usuarios" type="text" name="email">
   </td>
  </tr>
  <tr>
   <td>
    <label for="login">Login de usuário: </label>
   </td>
   <td align="left">
    <input class ="form-usuarios" type="text" name="login">
   </td>
  </tr>
  <tr>
   <td>
    <label for="pass">Senha: </label>
   </td>
   <td align="left">
    <input class ="form-usuarios" type="password" name="pass">
   </td>
  </tr>
  <tr>
   <td>
    <label for="passconfirm">Confirme a senha: </label>
   </td>
   <td align="left">
    <input class ="form-usuarios" type="password" name="passconfirm">
   </td>
  </tr>
 </table>
<br />
<div class="field-button"; align="center"; style="margin-bottom:30px">
	<button type="submit"; style="width:150px; height:50px">  Enviar</button>
	<button type="reset"; style="width:150px; height:50px" >  Limpar</button>
</div>
</form>
   <?php   $Main->element('rodape');  ?>
   </body>
</html>
