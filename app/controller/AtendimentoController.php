<?php


class AtendimentoController extends App_Controller 
{

    public function __construct () 
    {
       parent::__construct(); 
       Load::loadModel('CategoriaModel');

    }

    public function index() 
	{   
	   
		$this->set('title','Home');
		$this->set('main',new MainViewHelper()); 
	    $this->view = 'atendimento';
	   
    }

    public function cadastrar()
	{
       
		if (!empty($this->data)) {			
            $data = $this->validationBasic($this->data);
		     
			if (empty($data['nome'])) {
					$this->setAlert('Nome obrigatorio','index');
			}
            if (empty($data['email'])) {
					$this->setAlert('Email obrigatorio', 'index');
			}  			
			
			if (empty($data['assunto'])) {
					$this->setAlert('Assunto obrigatorio', 'index');
			} 
			if ($this->AtendimentoModel->save($this->data)) {
				$this->setAlert('Mesagem Cadastrada com Sucesso !', 'index');
			} 
			
			$this->setAlert('Erro no cadastro !', 'index');
		}
		
    }
	
	
	private function validationBasic($data)
	{
      		
		$filters = [
		     'nome'     => [
				"filter" => FILTER_CALLBACK,
				"flags"  => FILTER_FORCE_ARRAY,
				"options"=> "ucwords"
			  ],
			 'assunto'  => FILTER_SANITIZE_STRING,
			 'email'    => FILTER_VALIDATE_EMAIL ,	
		     'mensagem' => FILTER_SANITIZE_STRING 
		] ;
		
		
		return  filter_var_array($data, $filters);
	}
	
	 

}