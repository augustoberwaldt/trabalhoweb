<?php

class ProdutosController extends Controller {

    
    public function index($id=null){
		 $produtos = [];
		 if (isset($this->data['busca'])) {
		       $produtos = $this->ProdutosModel->find([
				
					'filtro' => ["titulo ILIKE '%" . addslashes($this->data['busca'])  ."%'" , "OR descricao like '%" . addslashes($this->data['busca'])  ."%'" ]
			   ]);
			  
               if (empty($produtos)) {   		   
				  $this->view = 'busca_produtos_nao_econtrados';
			      return;
			   }
		 } else {                 
             Load::loadModel('ProdutosModel');
			 $ids = [];
             if (isset($this->data['produtos']))
                $ids =  array_column($this->data['produtos'], 'id') ;
			 $filtro = [];
			 if ($ids) {
			   $filtro['filtro'] = 'id in (' . implode(',',$ids). ')';
			 } 
			 
			 $produtos = $this->ProdutosModel->find($filtro); 
			
		}
		$this->setVars(compact('produtos'));
		$this->view = 'produtos';
	}
    public function remover($id = null) {
        $this->view = null;
		printrx('not implemented');
    }

	public function detalhe() {
        $this->view = 'produto';
		
        Load::loadClass('Produto');

		$produto = $this->ProdutosModel->find([
            'filtro'=> ['id'=> $this->data['produtos'][0]['id']]
		]);
	
		$produto = $produto[0];
	
	    $this->setVars(compact('produto'));
	    
    }
	
}