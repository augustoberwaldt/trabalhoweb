<?php


class IndexController extends App_Controller
{
  public function __construct () 
  {
    parent::__construct(); 
    Load::loadModel('CategoriaModel');

  }

  public function index()
	{
    $title = 'Home';
    Load::loadModel('ProdutosModel');
    $produtos = $this->ProdutosModel->find([
          'limite'=> 5
    ]);
	
    $maisvendidos = $this->ProdutosModel->find([
          'limite'  => 5,
          'filtro' => ['destaque'=> 0],
          'ordem'   => 'random()'
    ]);  

	$this->view ='index';
    $this->setVars(compact('title', 'produtos', 'maisvendidos'));
  }

  public function salva()
	{
        $this->view = NULL;
  }

}
