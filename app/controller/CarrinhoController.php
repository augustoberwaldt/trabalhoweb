<?php


class CarrinhoController extends App_Controller 
{

    
    public function index() 
	{   
		$title = 'Carrinho';
	    $this->view = 'carrinho';
    }

    public function salva()
	{
		Load::loadModel('ProdutosModel');
        $aux = array(
		'produto' => $this->data["titulo"], 
		'quantidade' => $this->data["qtd"],
		'preco' => $this->data["preco"],
		'subtotal' => $this->data["preco * qtd"],
		'total' => $this->data["val"],
		);
		$this->ProdutosModel->save($aux);
    }

	public  function adicionar()
	{
		
       $id_produto = $this->data['id'];
	   $qtd = $this->data['qtd'];
  
          
       if (empty($id_produto) ||  empty($qtd) ) {
		   
		   //valida  empty id
	   } 
	   
	   Load::loadModel('ProdutosModel');
	   
	   $produto = $this->ProdutosModel->find([
	           'filtro' => ['id' => $id_produto]
	   ]);
	   
	   if (empty($produto)) {
		   
		   //csas
	   }
	  
	   $_SESSION['carrinho']['produtos'][$id_produto] = $produto[0];   
       $_SESSION['carrinho']['produtos'][$id_produto]['qtd'] = $qtd;	   
	  
	   $this->redirect(['controller'=> 'carrinho' , 'action'=> 'index']);	
	}
	
	public function destroy() 
	{
		unset($_SESSION['carrinho']); 
		$this->redirect(['controller'=> 'carrinho' , 'action'=> 'index']);	
	}
	
	
	
}