<?php


class PagamentoController extends App_Controller
{
	public function __construct () 
  {
    parent::__construct(); 
    Load::loadModel('CategoriaModel');
    Load::loadViewHelper('MainViewHelper');
  }

  public function index()
  {
     
    $title = 'Pagamento';
    $this->view ='pagar';
    $this->setVars(compact('title'));

  }
 
  public function pagar()
  {
	  Load::loadModel('PedidosModel');
	  
	  if(empty($this->data["numero"])){
			$this->setAlert('Numero obrigatorio','index');	
		}
	  $aux = array(
		'bandeira' => $this->data["bandeira"], 
		'numero' => $this->data["numero"],
		'forma' => $this->data["pagar"],
		'id_produtos'=>$this->data["idprodutos"],
		'total'=>$this->data["valorTotal"],
		
		
  );
  	$auxil=$this->PedidosModel->save($aux);
	if($auxil){
		unset($_SESSION['carrinho']);
		$this->setAlert('Pagamento realizado com sucesso','index');	
	}else{
		$this->setAlert('Erro! Não foi possível fazer o pagamento. Verifique os dados informados.','index');	
	}

  }
}
