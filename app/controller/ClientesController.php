<?php
use  Vendor\classes\Test\Teste;

class ClientesController extends App_Controller
{
  public function __construct () 
  {
    parent::__construct(); 
    Load::loadModel('CategoriaModel');
    Load::loadViewHelper('MainViewHelper');
  }

  public function index()
  {
    $title = 'Cadastro Cliente';
		$this->view ='clientes_cadastro';
    $this->setVars(compact('title'));
  }

  public function salvar()
  {
	    Load::loadModel('ClientesModel');
	    // busca no banco
		//printrx($this->data);
		
		if(empty($this->data["nome"])){
			$this->setAlert('Nome obrigatorio','index');	
		}
		if(empty($this->data["cep"])){
			$this->setAlert('Rua obrigatorio','index');	
		}
		if(empty($this->data["email"])){
			$this->setAlert('Email obrigatorio','index');	
		}
		if(empty($this->data["pass"])){
			$this->setAlert('Senha obrigatorio','index');	
		}
		if(empty($this->data["login"])){
			$this->setAlert('Login obrigatorio','index');	
		}
		if(empty($this->data["passconfirm"])){
			$this->setAlert('Confirmação obrigatorio','index');	
		}
			
		$auxiliar = array(
            'nome' => $this->data["nome"], 
            'email' => $this->data["email"],
            'login' => $this->data["login"],
            'cep' => $this->data["cep"],
            'senha' => md5($this->data["pass"]),
            'rua' => $this->data["rua"],
            'numero' => $this->data["numero"],
            'complemento' => $this->data["complemento"],
            'bairro' => $this->data["bairro"],
            'cidade' => $this->data["cidade"],
            'sexo' => $this->data["sexo"],
            'dia' => $this->data["dia"],
            'mes' => $this->data["mes"],
            'ano' => $this->data["ano"],
            'fone' => $this->data["telefone"],
            'codigo' => $this->data["codarea"],
            'confirmar' => $this->data["passconfirm"]
		
		);
		if ($this->ClientesModel->save($auxiliar)) {
             $this->setAlert('Usuario Cadastrado com Sucesso!', 'index'); 
        }


  }

  public function logar()
  {
      if (!$this->data['Email']) {
          $this->setAlert('Email e obrigatorio','index');
      }
      if (!$this->data['Password']) {
          $this->setAlert('Senha e obrigatorio','index');
      }
      Load::loadModel('ClientesModel');
      $cliente = $this->ClientesModel->find([
            'filtro'=> [
               'email' => $this->data['Email'],
               'senha' => md5($this->data['Password'])
            ]
      ]);
      if (!$cliente) {
         $this->setAlert('Usuario não Cadastrado !', 'index'); 
      }
      $_SESSION['cliente']  = $cliente[0] ;
      $this->setAlert('Logado com sucesso !','../index'); 
  }
  
  public function logout()
  {
      unset($_SESSION['cliente']);
      $this->setAlert('sessão encerrada  !','../index'); 
  }
  
  
}
