<?php
class MainViewHelper
{
	 
	 public function element($element, array $vars = [])
	 {   
         extract($vars);
		 include VIEW. DS. 'elements'.DS . $element.'.php'; 
	 }
	 
	 public function  getBloco($param , $options = [])
	 {
         $model = ucfirst($param.'Model');
      
         if (!property_exists($this->_controller, $model)) {
                Load::loadModel($model);
         }
         return  $this->_controller->$model->find($options);
	 }
	 
     public function getCarrinho()
	 {
		 if (isset($_SESSION['carrinho'])) {
			 return $_SESSION['carrinho'];
		 }
		 return false;
	 }
	 public function getCliente()
	 {
		 if (isset($_SESSION['cliente'])) {
			 return $_SESSION['cliente'];
		 }
		 return false;
	 }
     
     public function getClienteLogado()
	 {
		 if (!empty($_SESSION['cliente'])) {
			 return true;
		 }
		 return false;
	 }
     public function getTotalCarrinho()
	 {
		 if (!empty($_SESSION['carrinho']['produtos'])) {
			 return count($_SESSION['carrinho']['produtos']);
		 }
		 return 0;
	 }
     function currency ($vlr, $symbol = true) {
		$vlr_r = ($symbol ? 'R$ ' : '') . number_format($vlr, 2, ',', '.');
		
		return $vlr_r;
	}
}